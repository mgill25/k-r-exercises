/*
 * In a two's complement number representation, our version of `itoa`
 * does not handle the largest negative number, that is, the value of n equal
 * to -(2 ^ (wordsize - 1)). Explain why not. Modify it to print that value correctly,
 * regardless of the machine on which it runs.
 */

/* 
 * Debugging the program, it seems that the problem is the fact that the largest integer,
 * INT_MIN, does not change its sign. This leads to the while condition being false, 
 * and the do-while executes only once.
 * Why does this happen? 
 * Because in the 2's compliment number system, you can't represent the absolute
 * value of the largest negative number. 
 * How to remedy this: 
 *  1. while (n /= 10)  
 *  2. abs(n % 10) + '0' 
 * ref: http://clc-wiki.net/wiki/K%26R2_solutions:Chapter_3:Exercise_4
 */

#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <stdlib.h>

void reverse(char s[]) 
{
    int i;
    int j;
    char c;
    for (i = 0, j = strlen(s)-1; i < j; i++, j--) {
        c = s[i];
        s[i] = s[j];
        s[j] = c;
    }
}

void itoa(int n, char s[])
{
    int i, sign;

    if ((sign = n) < 0) {           // record sign
        n = -n;                     // make n positive
    }
    i = 0;
    
    printf("%d\n", n);

    // Using the do-while loop here, generate digits in reverse order.
    do {
        s[i++] = abs(n % 10) + '0';          // get next digit
    } while (n /= 10);            

    if (sign < 0) {
        s[i++] = '-';
    }
    s[i] = '\0';
    reverse(s);
}

int main()
{
    int number = INT_MIN;
    printf("Integer: %d\n", number);
    
    char string[20];
    itoa(number, string);
    printf("String: %s\n", string);

    return 0;
}    

