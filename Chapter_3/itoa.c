/*
 * itoa: Convert from Integer -> String
 * Slighly more complicated that might be thought at first.
 * Easy method generates them in the wrong order.
 * We generate the string backwards, then reverse it. 
 */

#include <stdio.h>
#include <string.h>
#include <limits.h>

void reverse(char s[]) 
{
    int i;
    int j;
    char c;
    for (i = 0, j = strlen(s)-1; i < j; i++, j--) {
        c = s[i];
        s[i] = s[j];
        s[j] = c;
    }
}

void itoa(int n, char s[])
{
    int i, sign;

    if ((sign = n) < 0) {           // record sign
        n = -n;                     // make n positive
    }
    i = 0;
    
    printf("%d\n", n);

    // Using the do-while loop here, generate digits in reverse order.
    do {
        s[i++] = n % 10 + '0';          // get next digit
    } while ((n /= 10) > 0);            // delete it

    if (sign < 0) {
        s[i++] = '-';
    }
    s[i] = '\0';
    reverse(s);
}

int main()
{
    int number = INT_MIN + 1;                       // Algo won't handle INT_MIN (largest negative number). Why? 
    printf("Integer: %d\n", number);
    
    char string[20];
    itoa(number, string);
    printf("String: %s\n", string);

    return 0;
}    

