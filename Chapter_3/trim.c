#include <stdio.h>
#include <string.h>

/* trim: remove trailing blanks, tabs, newlines */
int trim(char s[])
{
    int n;
    for (n = strlen(s) - 1; n >= 0; n--) {
        if (s[n] != ' ' && s[n] != '\t' && s[n] != '\n') {
            break;
        }
    }
    s[n+1] = '\0';
    return n;
}

int main()
{
    char name[20] = "Manish   ";
    char empty[10] = "";
    printf("Original len(%s): %d\n", name, (int)strlen(name));
    printf("Original len(%s): %d\n", empty, (int)strlen(empty));
    trim(name);
    int n2 = trim(empty);
    printf("Trimmed(%s): %d\n",name, (int)strlen(name));
    printf("Trimmed(%s): %d\n", empty, n2);    
    return 0;
}
