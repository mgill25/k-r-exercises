/* Our binary search makes two tests inside the loop,
 * when one would suffice (at the price of more tests outside).
 * Write a version with only one test inside the loop,
 * and measure the difference in run-time.
 */

/* COMPILE WITH THE -lm FLAG TO LINK THE MATH.H LIBRARY 
 * gcc -Wall 3-1.c -o 3-1 -lm 
 * */
#include <stdio.h>

int binsearch(int x, int arr[], int len)
{
    int low = 0; 
    int high = len - 1;
    int mid = (low + high) / 2;

    while (low <= high && x != arr[mid]) {
        if (x < arr[mid]) {
            high = mid - 1;
        }
        else {
            low = mid + 1;
        }
        mid = (low + high) / 2;
    }
    
    if (x == arr[mid]) {
        return mid;
    }
    else {
        return -1;
    }
}

int main()
{
    int array[10] = { 0, 10, 20, 34, 56, 78, 89, 92, 134, 567 };
    int x = 666;
    int i; 

    for (i = 0; i < 10; ++i) {
        printf("%3d ", array[i]);
    }
    printf("\n");
    
    int pos = binsearch(x, array, 10);
    printf("Searching for %d...\n", x);

    if (pos != -1) {
        printf("Position of %d:\t%d\n", x, pos);
    }
    else {
        printf("Not found. :(\n");
    }

    return 0;
}
