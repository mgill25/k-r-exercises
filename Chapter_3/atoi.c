#include <stdio.h>

int atoi(char s[]);

int main()
{
    // main
    printf("Converting a string to an integer...\n");
    char string[10] = "123456789";      // a string (character array)

    printf("String : %s\n", string);
    int numerical = atoi(string);
    printf("Numerical : %d\n", numerical);

    printf("--------------\n");
    char name[20] = "Manish Gill";
    int num_name = atoi(name);
    printf("String : %s\n", name);
    printf("Numerical : %d\n", num_name);
    return 0;
}

int atoi(char s[])
{
    // Convert a string s to an integer.
    int i, n;
    n = 0;

    for(i = 0; s[i] >= '0' && s[i] <= '9'; ++i) {
        printf("n : %d\n", n);
        n = 10 * n + (s[i] - '0');
    }

    return n;
}
