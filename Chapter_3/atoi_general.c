/* 
 * atoi function - copes with leading whitespace, and an optional + or -
 * sign.
 *          1. Skip whitespace, if any.
 *          2. Get sign, if any.
 *          3. get integer part and convert it.
 */

#include <stdio.h>
#include <ctype.h>

int atoi(char s[])
{
    int i, n, sign;
    
    // first, we skip any leading whitespaces.
    for (i = 0; isspace(s[i]); i++)
        ;                               // do nothing
    
    // get sign, if any
    sign = (s[i] == '-') ? -1 : 1;
    if (s[i] == '+' || s[i] == '-') {
        i++;                            // skip sign as well.
    }
    
    // now convert to int
    for (n = 0; isdigit(s[i]); i++) {
        n = n * 10 + (s[i] - '0');
    }

    return sign * n;
}

int main()
{
    // main
    printf("Converting a string to an integer...\n");
    char string1[10] = "123456789";      // a string (character array)
    char string2[10] = "  -34";
    char s3[10] = "+45";

    printf("String : %s\n", string1);
    int numerical = atoi(string1);
    printf("Numerical : %d\n", numerical);

    printf("String: %s\n", string2);
    printf("Numerical: %d\n", atoi(string2));
    printf("String: %s\n", s3);
    printf("Numerical: %d\n", atoi(s3));

    printf("--------------\n");
    char name[20] = "Manish Gill";
    int num_name = atoi(name);
    printf("String : %s\n", name);
    printf("Numerical : %d\n", num_name);
    return 0;
}

