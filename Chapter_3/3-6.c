/*
 * Write a version of itoa that accepts three arguments instead of two. 
 * The third argument is a minimum field width; the converted number must
 * be padded with blanks on the left if necessary to make it wide enough.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void reverse(char s[])
{
    int i, j;
    int c;
    for (i = 0, j = strlen(s) - 1; i < j; i++, j--) {
        c = s[i];
        s[i] = s[j];
        s[j] = c;
    }
} 

void itoa(int n, char s[], int w) 
{
    int i, sign;
    if ((sign = n) < 0) {
        n = -n;
    }
    
    i = 0;
    do {
        s[i++] = abs(n % 10) + '0';
    } while (n /= 10);
    
    if (sign < 0) {
        s[i++] = '-';
    }
    
    while (strlen(s) < w) {
        s[i++] = ' ';
    }

    s[i] = '\0';
    reverse(s);
}

int main()
{
    int number = 148;
    char string[100];

    printf("Integer: %d\n", number);    
    itoa(number, string, 10);
    printf("String: %s\n", string);    

    return 0;
}
