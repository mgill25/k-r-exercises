/*
 * Write a function `itob(n, s, b)` that converts the integer n into a base b character
 * representation in the string s. In particular, itob(n, s, 16) formats n as a hexadecimal
 * integer in s.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>

void reverse(char s[]) 
{
    int i, j, c;
    for (i = 0, j = strlen(s) - 1; i < j; i++, j--) {
        c = s[i];
        s[i] = s[j];
        s[j] = c;
    }
}

void itob(int n, char s[], int b)
{
    int i, sign, tmp;
    if ((sign=n) < 0) {
        n = -n;
    }
    i = 0;
    do {
        tmp = n % b;
        s[i++] = (tmp <= 9) ? (tmp + '0') : (tmp + 'a' - 10);
    } while ((n /= b) > 0);
    
    /*
     * ASCII manipulation. Hint: 
     * for i in range(10, 16):
     *     print chr(i + ord('a') - 10) + ' ',
     * gives: a b c d e f
     */

    if (sign < 0) {
        s[i++] = '-';
    }
    s[i] = '\0';
    reverse(s);
}

int main()
{
    int number = 123;                       // Algo won't handle INT_MIN (largest negative number). Why? 
    printf("Integer: %d\n", number);
    
    char string[200] = {0};
    itob(number, string, 16);
    printf("String: %s\n", string);

    return 0;
}    
