/*
 * Write a function `expand(s1, s2)` that expands shorthand notations
 * like a-z in the string s1 into the equivalent complete list abc...xyz
 * in s2. Allow letters of either case and digit, and be prepared to
 * handle cases like a-b-c and a-z0-9 and -a-z. Arrange that a leading
 * or trailing - is taken literally.
 */

// Dare I say Regular Expressions?

#include <stdio.h>
#include <ctype.h>
#include <string.h>

void expand(char s1[], char s2[])
{
    // First, we skip leading whitespaces
    int i = 0;
    int j = 0;

    for (i = 0; isspace(s1[i]); i++)
        ;

    if (s1[i] == '-') {
        s2[0] = '-';
        i++;
    }

    int incount = i;                                    // counter for the inner loop
    while (s1[i] && i < strlen(s1)) {
        if (s1[i] == '-') {
            char upper = s1[i-1];
            char lower = s1[i+1];

            if (upper <= lower) {
                if (s2[incount - 1] == upper) {
                    upper++;            // to skip over the duplicates (a-b-c should be abc, not abbc)
                }

                //printf("upper: %c\tlower: %c\n", upper, lower);
                for (j = upper; j <= lower && isalnum(j); j++) {
                    //printf("Incount: %d", incount);
                    s2[incount] = j;
                    //printf(" %c\n", s2[incount]);
                    incount++;
                }
            }
        }
        i++;
    }

    if (s1[strlen(s1)-1] == '-') {
        s2[incount] = '-';
        incount++;
    }

    s2[incount] = '\0';
}

int main()
{
    char s1[10] = "a-R-L";
    char s2[100];

    expand(s1, s2);
    printf("%s\n", s1);
    printf("%s\n", s2);
    printf("-----------------\n");
    
    char s3[10] = "a-g-h";
    char s4[100];
    expand(s3, s4);
    printf("%s\n", s3);
    printf("%s\n", s4);
    printf("-----------------\n");
    
    return 0;
}
