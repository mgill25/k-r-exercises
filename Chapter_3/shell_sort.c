/*
 * Invented in 1959 by D.L. Shell.
 * Basic idea is that in the early stages, far apart elements
 * are compared, rather than adjacent ones. This tends to 
 * eliminate large amounts of disorder quickly, so later stages
 * have less work to do. Interval b/w compared elems is gradually
 * decreased to 1, at which point, the sort effectively becomes
 * an adjacent exchange method.
 */

#include <stdio.h>

// Sort v[0], v[1]...v[n-1] into increasing order
void shellsort(int v[], int n)
{
    int gap, i, j, temp;

    for (gap = n/2; gap > 0; gap /= 2) {
        //printf("\nCurrent Gap: %d\n", gap);
        for (i = gap; i < n; i++) {
            //printf("\ni: %d\t", i);
            for (j = i - gap; j >= 0 && v[j] > v[j+gap]; j -= gap) {
                //printf("\n\tj: %d\tj+gap: %d\t", j, j+gap);
                temp = v[j];
                v[j] = v[j+gap];
                v[j+gap] = temp;
            }
        }
    }
    printf("\n");
}

int main()
{
    int arr[7] = { 87, 234, 76, 12, 89, 15, 56 };

    printf("Unsorted Array\n");
    for (int i = 0; i < 7; ++i) {
        printf("%d ", arr[i]);
    }
    printf("\n");

    shellsort(arr, 7);

    printf("Sorted Array\n");
    for(int i = 0; i < 7; ++i) {
        printf("%d ", arr[i]);
    }
    printf("\n");

    return 0;
}
