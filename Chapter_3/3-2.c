/* Write a function escape(s, t) that converts characters like
 * newline and tab into visible escape sequences like \n and \t
 * as it copies the string s to t. Use a switch. Write a function
 * for the other direction as well, converting escape sequences
 * into real characters.
 */

#include <stdio.h>

void escape(char s[], char t[])
{
    int i = 0;
    int j = 0;
    // I've only take the two most common escape sequences: tabs and newlines.
    // other cases can be handled similarly.
    while (s[i]) {
        switch (s[i]) {
            case '\t':
                t[j] = '\\';
                t[++j] = 't';
                break;
            case '\n':
                t[j] = '\\';
                t[++j] = 'n';
                break;
            default:
                t[j] = s[i];
                break;
        }
        i++;
        j++;
    }
    // copy the null character
    t[j] = s[i];
}

void unescape(char s[], char t[])
{
    // convert escape sequences into real characters.
    int i = 0;
    int j = 0;
    while ((t[j] = s[i]) != '\0') {
        switch (s[i]) {
            case '\\':
                switch(s[++i]) {
                    case 'n':
                        t[j] = '\n';
                        break;
                    case 't':
                        t[j] = '\t';
                        break;
                }
        }
        i++;
        j++;
    }
}

int main()
{
    char s[100] = "Manish Gill\t\tis an amazing fellow!\n\n";     // string with tabs and spaces;
    char t[300];
    escape(s, t);
    printf("Original: %s\n", s);
    printf("Escaped: %s\n", t);
    printf("\n\n");
    char foo[100] = "This is\\tan \\nescaped string!\\n";
    char bar[100];
    unescape(foo, bar);
    printf("Original: %s\n", foo);
    printf("Unescaped: %s\n", bar);
    return 0;
}
        
    

