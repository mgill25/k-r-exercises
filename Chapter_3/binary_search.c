#include <stdio.h>

/* binsearch: find x in v[0] <= v[1] <= .... v[n-1] (Ordered Array) */
int binsearch(int x, int v[], int n)
{
    int low, mid, high;
    low = 0;
    high = n - 1;
    while (low <= high) {
        mid = (low + high) / 2;
        if (x < v[mid]) {
            high = mid - 1;
        }
        else if (x > v[mid]) {
            low = mid + 1;
        }
        else {
            // found match
            return mid;
        }
    }

    return -1;
}

int main()
{
    int array[10] = { 0, 10, 20, 34, 56, 78, 89, 92, 134, 567 };
    int x = 34;
    int i; 

    for (i = 0; i < 10; ++i) {
        printf("%3d ", array[i]);
    }
    printf("\n");
    
    int pos = binsearch(x, array, 10);

    if (pos != -1) {
        printf("Position of %d:\t%d\n", x, pos);
    }
    else {
        printf("Not found. :(\n");
    }

    return 0;
}
