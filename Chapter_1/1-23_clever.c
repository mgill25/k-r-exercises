/* 
 * My attempt to do some clever tricks with K&R problem 1.23 (Removing comments from a C program)!
 */

// TODO: See if there's another way that doesn't uses flags.
#include <stdio.h>

int main()
{
    int c;
    int flag = 0;
    char buffer[2] = { 0 };     // Buffer to store comment symbols

    while ((c = getchar()) != EOF) {
        // printf("flag -- %d\nc -- %c\n", flag, c);
        if (c == '/') {
            buffer[0] = c;
        }

        if (buffer[0] == '/' && (c == '*' || c == '/')) {
            flag = 1;       // Comment starts
            buffer[0] = buffer[1] = 0;      // clear buffer
        }
        
        if (flag == 1) {
           if (c == '*' || c == '\n') {
              buffer[0] = c;
           }

           if ((buffer[0] == '*' && c == '/') || c == '\n') {
               flag = 0;        // Comment ended
           }
        } 

        if (flag == 0) {
            putchar(c);
        }
    } 
    
    return 0;
}
    
 
