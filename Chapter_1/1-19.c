/* Write a function reverse(s) that reverses the character string s. Use it to write a program that reverses
its input a line at a time. */
#include <stdio.h>
#define MAXLENSIZE 1000

int get_line(char line[], int limit);
void reverse(char s[]);

int main(int argc, char const *argv[])
{
    int c;
    char line[MAXLENSIZE];

    while ((c = get_line(line, MAXLENSIZE)) > 0) {
        // while we are getting an input, reverse that input and print it.
        reverse(line);
        printf("%s", line);
    }
    return 0;
}

int get_line(char line[], int limit)
{
    int i, c;
    for(i=0; i < limit -1 && (c = getchar()) != EOF && c != '\n'; ++i) {
        line[i] = c;
    }

    if (c == '\n') {
        line[i] = c;
        ++i;
    }
    line[i] = '\0';
    return i;
}

void reverse(char line[])
{
    int i = 0;
    int j = 0;
    char temp;
    // First, calculate the length of the line
    for(j = 0; line[j] != '\0'; ++j)
        ;
    --j;

    // Use if you want to discount newlines.
    /*
    if (line[j] == '\n') {
        line[j] = '\0';
        --j;
    }
    */
    for(i = 0; i < j; ++i) {
        temp = line[i];
        line[i] = line[j];
        line[j] = temp;
        --j;
    }
}
