/*
 * Write a program detab that replaces tabs in the input with the proper number of blanks to space to the next tab stop.
 * Assume a fixed set of tab stops, say every n columns. Should n be a variable or a symbolic parameter?
 */

#include <stdio.h>

#define MAXLENSIZE 1000
#define TAB '\t'
#define SPACE ' '
#define TABSTOP 4

int get_line(char line[], int limit);
void detab(char line[], int limit);
void detab2(char to[], char from[]);

int main() 
{
    int c;
    char line[MAXLENSIZE];
    char saved[MAXLENSIZE];

    while ((c = get_line(line, MAXLENSIZE)) > 0) {
        //detab(line, c);
        detab2(saved, line);
        printf("%s", saved);
    }


    return 0;
}

int get_line(char line[], int limit) 
{
    int i, c;
    for(i = 0; i < limit - 1 && (c = getchar()) != EOF && c != '\n'; ++i) {
        line[i] = c;
    }

    if(c == '\n') {
        line[i] = c;
        ++i;
    }

    line[i] = '\0';
        
    return i;
}

void detab(char line[], int limit)
{
    int i, j;
    for(i = 0; i < limit - 1; ++i) {
        if (line[i] == TAB) { 
            // And this is where the magic happens.
            for (j = 0; j < TABSTOP; ++j) {
                putchar(SPACE);
            }
        }
        else if(line[i] == '\n') {
                printf("\n");
        }
        else {
            printf("%c", line[i]);
        }
    }
}

void detab2(char to[], char from[]) 
{
    int i, j, n;
    i = j = n = 0;

    while ((to[j] = from[i]) != '\0') {
        if (to[j] == '\t') {
            for(n = 0; n < TABSTOP; ++n, ++j) {
                to[j] = SPACE; 
            }
        }
        else {
            ++j;
        }
        ++i; 
    }
}
