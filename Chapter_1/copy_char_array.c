/*
 * K&R Section 1.9
 * Copying lines from a character array.
 */
#include <stdio.h>
#define MAXLINESIZE 1000

int get_a_line(char line[], int maxlinesize);
void copy(char from[], char to[]);

/*
 * Print longest input line!
 */
int main()
{
    int len;                    // Current line length
    int max;                    // Maximum length seen so far
    char line[MAXLINESIZE];     // Current input line
    char longest[MAXLINESIZE];  // Longest input saved here

    max = 0;
    while ((len = get_a_line2(line, MAXLINESIZE)) > 0) {
        if (len > max) {
            max = len;
            copy(line, longest);
        }
    }

    if (max > 0) {
        // There was a line.
        printf("The longest line...\n\t%s", longest);
    }

    return 0;
}

int get_a_line(char s[], int limit)
{
    int c, i;

    for (i=0; i < limit - 1 && (c = getchar()) != EOF && c != '\n'; ++i) {
        s[i] = c;
    }

    if (c == '\n') {
        s[i] = c;
        ++i;
    }
    s[i] = '\0';
    return i;                   // Just return the length of the line.
}

int get_a_line2(char s[], int limit)
{
    int i, c;
    i = 0;
    while(i < limit - 1 && (c = getchar()) != EOF && c != '\n') {
        s[i] = c;
        ++i;
    }
    if (c == '\n') {
        s[i] = c;
        ++i;
    }

    s[i] = '\0';
    return i;

}

void copy(char from[], char to[])
{
    int i;
    i = 0;
    while ((to[i] = from[i]) != '\0') {
        ++i;
    }
}
