#include <stdio.h>
int main()
{
    /* Count lines in input */
    int c, newlines, tabs, blanks;

    newlines = 0;
    tabs = blanks = 0;
    while ((c = getchar()) != EOF){
        if (c == '\n'){
            ++newlines;
        } else if (c == '\t'){
            ++tabs;
        } else if (c == ' '){
            ++blanks;
        }
    }

    printf("Total number of lines: %d\n", newlines);
    printf("Total number of tabs: %d\n", tabs);
    printf("Total number of blanks: %d\n", blanks);

    return 0;
}

