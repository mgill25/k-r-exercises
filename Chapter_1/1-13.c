/* K&R Exercise 1-13. Printing a Histogram of the lengths of words in its input.
 * Can be done both horizontally and vertically
 */
#include <stdio.h>
#define IN 1
#define OUT 0

int main()
{
    int c, nword = 0;
    int index = 0;
    int words[10] = { 0 };
    int state = OUT;

    // Only limited to words of maximum length of 10.
    // Index of the array represents the word length, and value represents the count.
    // For example, words[3] = 4 means there are 4 words of length 3.
    while ((c = getchar()) != EOF) {
        if (state == IN) {
            nword++;
            // printf("%d\n", nword);
        }
        if (c == ' ' || c == '\n' || c == '\t'){
            words[nword] += 1;
            nword = 0;          // reset nword.
            state = OUT;
        }
        else if (state == OUT){
            state = IN;
        }
    }

    printf("Statistics\n");
    printf("--------\n");
    for(index=1; index < 10; index++){
        printf("%d word(s) of length %d\n", words[index], index);
    }
    printf("--------------------------------------------------\n");

    for(int i=1; i<10; i++) {
        printf("%d ", words[i]);
    }

    /* Now, we create a histogram! */

    /* Horizontal bars */
    printf("\nHorizontal bars\n");
    printf("---------------\n");

    for(index=1; index < 10; index++) {
        int count = words[index];
        printf("|%d|  ", index);
        for(int j=0; j < count; j++) {
            // printf("▆");
            printf("*");
        }
        printf("\n");
    }

    printf("--------------------------------------------------\n");

    /* Vertical bars */
    /* First calculate the maximum length. Start counting down, and for each count,
     * loop for the number of words (1 to 10). If words[index] is equal to or less than the current count,
     * print the *, otherwise, print space
     */

    printf("\nVertical bars\n");
    printf("-------------\n");

    int max_count = words[1];
    for (int i=1; i < 10; i++){
        if (max_count < words[i]) {
            max_count = words[i];
        }
    }

    printf("max_count: %d\n", max_count);
    for (int i=max_count; i>0; i--) {
        printf("%d:\t", i);
        for(int j=1; j<10; j++) {
            if(i <= words[j]) {
                printf(" *");
            }
            else {
                printf("  ");
            }
        }
        printf("\n");
    }
    printf("\t 1 2 3 4 5 6 7 8 9\n");

    return 0;
}
