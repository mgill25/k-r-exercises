/*
 * Write a program to check a C program for rudimentary syntax errors like unbalances parentheses, brackets, braces.
 * Don't forget about quotes, both single and double, escaped sequences, and comments. (This program is hard if you do it in full generality.)
 */
/*
 * TODO: escaped quotes make the number un-even and produce error.
 * TODO: Comment check support!
 */

#include <stdio.h>

#define MAXLENSIZE 1000

int get_line(char line[], int limit);
int iseven(int n);

int main()
{
    int braces, parens, brackets, single_quotes, double_quotes;
    braces = parens = brackets = double_quotes = single_quotes = 0;
    char line[MAXLENSIZE];
    int len;
    while ((len = get_line(line, MAXLENSIZE)) > 0) {
        int i = 0;
        // printf("%s", line);
        while (i < len) {
            if (line[i] == '[') {
                brackets++;
            }
            if (line[i] == ']') {
                brackets--;
            }
            if (line[i] == '(') {
                parens++;
            }
            if (line[i] == ')') {
                parens--;
            }
            if (line[i] == '{') {
                braces++;
            }
            if (line[i] == '}') {
                braces--;
            }
            if ((line[i] == '\\' || line[i] == '\'') && line[i+1] == '"') {
                double_quotes--;
            }
            if (line[i] == '\\' && line[i+1] == '\'') {
                single_quotes--;
            }
            if (line[i] == '"') {
                double_quotes++;            //  can also do double_quotes *= -1, if double_quotes is initialized = 1.
            }
            if (line[i] == '\'') {
                single_quotes++;            // same here. This will toggle the values b/w 1 and -1.
            }
            
            i++;
        }
    }

    if (brackets != 0) {
        printf("[Error]: Mismatching bracket!\n");
    }
    if (braces != 0) {
        printf("[Error]: Mismatching braces!\n");
    }
    if (parens != 0) {
        printf("[Error]: Mismatching parenthesis!\n");
    }
    if (iseven(double_quotes) != 0) {                       // then, instead of checking even, check if value == 1
        printf("[Error]: Mismatching double quotes!\n");
    }
    if (iseven(single_quotes) != 0) {
        printf("[Error]: Mismatching single quotes!\n");
    }


    if (parens ==  0 && brackets == 0 && braces == 0 && brackets == 0 && iseven(single_quotes) == 0 && iseven(double_quotes) == 0) {
        printf("No errors found!\n");
    }

    return 0;
}


int get_line(char line[], int limit)
{
    int c, i;
    for(i = 0; i < limit-1 && (c = getchar()) != EOF && c != '\n'; ++i) {
        line[i] = c;
    }

    if (c == '\n') {
        line[i] = c;
        ++i;
    }

    line[i] = '\0';

    return i;
}

int iseven(int n) {
    return n % 2;
}
