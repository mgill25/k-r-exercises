/*
 * Program to remove all comments from a C Program. Don't forget to handle 
 * quoted strings and character constants properly.
 * C comments do not nest.
 */
// TODO: Remove weird newline error. Newline characters are replaced by ^@ in the output. 
// TODO: If escaped inverted-comma inside quoted strings, doesn't work properly.
// TODO: If inverted comma before a string starts, flag sets pre-maturely, and doesn't work properly. 
#include <stdio.h>

#define MAXLENSIZE 1000
int flag = 0;
int single = 0;                // flag to indicate single-line comments
int strflag = 0;               // indicate quoted strings

int get_line(char line[], int limit);
void remove_comments(char line[], int limit);

int main()
{
    char line[MAXLENSIZE];
    int c;
    while ((c = get_line(line, MAXLENSIZE)) > 0) {
        remove_comments(line, c);
        printf("\n");
    }
    return 0;
    printf("hello /* This is not a comment! */ dude!"); /* Now this is an actual comment! */
}

int get_line(char line[], int limit)
{
    int c, i;
    for(i = 0; i < limit-1 && (c = getchar()) != EOF && c != '\n'; ++i) {
        line[i] = c;
    }

    if (c == '\n') {
        line[i] = c;
        ++i;
    }

    line[i] = '\0';

    return i;
}

void remove_comments(char line[], int limit)
{
    // This function will remove all the comments from a line in the program.
    int i = 0;
    for(i = 0; i < limit - 1; ++i) {
        /* printf(" line[i]: %c", line[i]); */
        /* printf("\tstrflag: %d\n", strflag); */
        if (line[i] == '"' && strflag == 0) {
            strflag = 1;        // Toggle when inside quote strings
        } else if (line[i] == '"' && strflag == 1) {
            strflag = 0;        // Toggle back
        }
        
        if (line[i] == '/' && strflag != 1) {
            if (line[i+1] == '*') { 
                // printf("Comment started!\n");
                flag = 1;
            }

            else if (line[i+1] == '/') {
                flag = 1;
                single = 1;
            }
        }

        if (flag == 1) {
            if (line[i] == '*' && line[i+1] == '/') {
                // printf("Comment Finished!\n");
                flag = 0;
                i += 2;
            } else if (single == 1 && line[i+1] == '\n') {
                // printf("newline!\n");
                flag = 0;
                i += 2;
                single = 0;
            }
        }

        if (flag == 0) {
            printf("%c", line[i]);
        }
    }
    // printf("\n");
    /* 
    if (flag == 0) {
        printf("\n");
    }
    */
}
