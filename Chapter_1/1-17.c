/* Write a program to print all input lines that are longer than 80 characters. */
#include <stdio.h>

#define MAXLENSIZE 1000

int get_line(char linearray[], int limit);
void copy(char from[], char to[]);

int main()
{
    int len, i = 0;
    char line[MAXLENSIZE];
    printf("Lines longer than 80 characters...\n");
    while ((len = get_line(line, MAXLENSIZE)) > 0) {
        if (len > 80) {
            ++i;
            printf("Line[%d]\t\t%s", i, line);
        }
    } 
    return 0;
}

int get_line(char s[], int limit)
{
    int c, i;
    for(i = 0; i < limit - 1 && (c = getchar()) != EOF && c != '\n'; ++i) {
        s[i] = c;
    }
    if (c == '\n') {
        s[i] = c;
        ++i;
    }
    s[i] = '\0';

    return i;
}

