/* 
 * Using getchar and putchar
 */
#include <stdio.h>
int main()
{
    int c;

    c = getchar();
    while (c != EOF) {
        putchar(c);
        c = getchar();          // Shouldn't the program stop here to wait for new input? 
        // printf("debug");     // ^ No, because we should also count the Return key's value in the buffer.
    }
    return 0;
}
