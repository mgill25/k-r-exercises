/* Write a program that takes the input and reverses it! */
#include <stdio.h>

#define MAXLENSIZE 1000

int get_line(char line[], int limit);
void reverse(char s[]);

int main()
{
    int c;
    char line[MAXLENSIZE];

    while ((c = get_line(line, MAXLENSIZE)) > 0) {
        reverse(line);                  // Just reverse the line.
        printf("%s", line);
    }

    return 0;
}

int get_line(char line[], int limit)
{
    int i, c;
    for(i = 0; i < limit - 1 && (c = getchar()) != EOF && c != '\n'; ++i) {
        line[i] = c;
    }

    if(c == '\n') {
        line[i] = c;
        i++;
    }

    line[i] = '\0';
    return i;
}

void reverse(char line[])
{
    int i, j;
    // first, calculate the line length.
    for(j = 0; line[j] != '\0'; ++j)
        ;

    --j;                // for the '\0'
    
    char temp; 
    
    for(i = 0; i < j; ++i) {
        temp = line[i];
        line[i] = line[j];
        line[j] = temp;
        --j;
    }
}

        

