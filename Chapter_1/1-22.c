/* K&R 1.22: Write a program to "fold" long input lines into two or more shorter lines
 * after the last non-blank character that occurs before the n-th column of input.
 * Make sure your program does something intelligent with very long lines, and if
 * there are no blanks or tabs before the specified column.
 */

#include <stdio.h>

#define COLUMN 50
#define MAXLENSIZE 1000

int get_line(char line[], int limit);
void fold(char line[], int limit);

int main()
{
    char line[MAXLENSIZE];
    int c;
    while ((c = get_line(line, MAXLENSIZE)) > 0) {
        fold(line, c);
    }

    return 0;
}

void fold(char line[], int limit)
{
   /* if line is shorter than column size, do nothing,
    * otherwise, fold!
    */
    if (limit <= COLUMN) {
        printf("%s", line);
        return;
    }

    if (limit > COLUMN) {
        int i,last_index, first_index;
        // get the index of the last and first non-blank character before and after the n-th column respectively.
        for(i = 0; i <= limit - 1; i++) {
            if (line[i] != ' ' && line[i] != '\t' && line[i] != '\n') {
                if (i < COLUMN)  {
                    last_index = i;
                }
                else if (i > COLUMN) {
                    first_index = i;
                    break;      // break as soon as the first index is found
                }
            }
        }

        // printf("last_index: %d, first_index: %d\n", last_index, first_index);

        for(i = 0; i < last_index + 1; i++) {
            printf("%c", line[i]);
        }
        printf("\n");
        int j;
        char saved[MAXLENSIZE];
        for(j = 0, i = first_index; i < limit - 1; i++, j++) {
            // printf("%c", line[i]);
            saved[j] = line[i];
        }
        fold(saved, j+2);       // recurse
    }
}

int get_line(char line[], int limit)
{
    int c, i;
    for(i = 0; i < limit-1 && (c = getchar()) != EOF && c != '\n'; ++i) {
        line[i] = c;
    }

    if (c == '\n') {
        line[i] = c;
        ++i;
    }

    line[i] = '\0';

    return i;
}

