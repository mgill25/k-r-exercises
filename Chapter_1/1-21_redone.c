#include <stdio.h>

#define TABLEN 4
#define MAXLENSIZE 1000
#define TAB '\t'
int get_line(char line[], int limit);
void entab(char line[], int limit);

int main()
{
    int c;
    char line[MAXLENSIZE];

    while ((c = get_line(line, MAXLENSIZE)) > 0) {
        entab(line, c);
    }

    return 0;
}

void entab(char line[], int limit) 
{
    int i, j;
    int spacecount = 0;
    for(i = 0; i < limit - 1; ++i) {
        if (line[i] == ' ') {
            spacecount++;
        }
        if (line[i] != ' ') {
            spacecount = 0;
        }
        if (spacecount == TABLEN) {
            // Now we have enough spacecount to replace them
            // with a tab.
            // Move back 3 characters
            i -= 3;
            limit -= 3;
            line[i] = TAB; 
            for(j = i + 1; j < limit; ++j) {
                line[j] = line[j+3];            // shift by 3 characters to the left
            }
            spacecount = 0;
            line[limit] = '\0';
        }
    }
    printf("%s", line);
}
            
int get_line(char line[], int limit)
{
    int c, i;
    for(i = 0; i < limit-1 && (c = getchar()) != EOF && c != '\n'; ++i) {
        line[i] = c;
    }

    if (c == '\n') {
        line[i] = c;
        ++i;
    }
    
    line[i] = '\0';

    return i;
}

