/*
 * The longest line program re-written using external variables.
 */
#include <stdio.h>

#define MAXLINE 1000

int max;

// Arrays of size MAXLINE preallocated here, will only store char items.
char line[MAXLINE];             // current input line
char longest[MAXLINE];          // longest line saved here

int get_line();
void copy();

int main()
{
    int len;

    // declare external variables.
    extern int max;
    extern char longest[];

    max = 0;
    while ((len = get_line()) > 0) {
        if (len > max) {
            max = len;
            copy();
        }
    }

    if (max > 0) {
        printf("%s", longest);
    }

    return 0;
}

int get_line()
{
    int c, i;
    extern char line[];

    for (i = 0; i < MAXLINE - 1 && (c = getchar()) != EOF && c != '\n'; ++i) {
        line[i] = c;
    }
    
    // Because we only went till just before newline in the above for loop,
    // explicitly handle newline here.
    if (c == '\n') {
        line[i] = c;
        ++i;
    }

    line[i] = '\0';     // added end character

    return i;
}

/*
 Function to copy items from the line array to the longest array
 */
void copy()
{
    int i = 0;
    extern char line[], longest[];
    while ((longest[i] = line[i]) != '\0') {
        ++i;
    }
}
