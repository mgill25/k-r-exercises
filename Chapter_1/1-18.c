/* Program to remove trailing blanks and tabs from line of input, and to delete entirely blank lines. */
#include <stdio.h>

#define MAXLENSIZE 1000

int get_line(char line[], int maxlinesize);

int main()
{
    char line[MAXLENSIZE];
    int c;
    int i;
    /* Get a line. If it's not entirely blank, remove trailing blanks (if they exist). */
    while ((c = get_line(line, MAXLENSIZE)) > 0) {
        // printf("length: %d\n", c);
        if (c == 1) {
            // printf("This be the completely blank line!\n"); // line is blank; don't output anything
            ;
        }
        else if (c > 1) {
            // got an output!
            for(i = c; i >= 0; i--) {
                if (line[i] == ' ' || line[i] == '\t' || line[i] == '\n') {
                    ;
                }
                printf("%c", line[c - i]);
            }

            /*
            for(i = c - 1; (line[i] == ' '|| line[i] == '\t' || line[i] == '\n'); i--)
                ;
            line[++i] = '\n';
            line[++i] = '\0';
            printf("%s", line);
            */
        }
    }

    return 0;
}

int get_line(char line[], int limit)
{
    int i = 0;
    int c;
    while (i < limit - 1 && (c = getchar()) != EOF && c != '\n') {
        line[i] = c;
        ++i;
    }

    if (c == '\n') {
        line[i] = c;
        ++i;
    }

    line[i] = '\0';
    // printf("returning...%d", i);
    return i;
}

