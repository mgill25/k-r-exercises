/* Improvements to the temperature conversion program by using defines and for loops */
#include <stdio.h>

#define START 0
#define STOP 300
#define STEP 20

int main()
{
    int fahr;
    float celsuis;         // int fahr will also work here, as int will automatically be converted to float 
    for (fahr=START; fahr <= STOP; fahr = fahr + STEP) {
       celsuis = (5.0/9.0)*(fahr - 32);
       printf("%3d\t%6.1f\n", fahr, celsuis);
    }

    return 0;
}
