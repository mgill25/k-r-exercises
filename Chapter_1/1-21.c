#include <stdio.h>

#define SPACE ' '
#define TAB '\t'
#define MAXLENSIZE 1000

int get_line(char line[], int limit);
void entab(char line[], int limit);

int main()
{
    char line[MAXLENSIZE];
    int c;

    while ((c = get_line(line, MAXLENSIZE)) > 0) {
        entab(line, c);            
    }

    return 0;
}

int get_line(char line[], int limit)
{
    int c, i;
    for(i = 0; i < limit-1 && (c = getchar()) != EOF && c != '\n'; ++i) {
        line[i] = c;
    }

    if (c == '\n') {
        line[i] = c;
        ++i;
    }
    
    line[i] = '\0';

    return i;
}

void entab(char line[], int limit)
{ 
    int i;
    i = 0;
    while (i < limit - 1) {
        if (line[i] == line[i+1] \
                && line[i+1] == line[i+2] \
                && line[i+2] == line[i+3] \
                && line[i+3] == line[i+4] \
                && line[i+4] == SPACE) {
            putchar(TAB);
            i = i + 4;
        } else if (line[i] == '\n') {
            printf("\n");
        } else {
            printf("%c", line[i]);
            i = i + 1;
        }
    }
}

