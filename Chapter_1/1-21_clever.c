#include <stdio.h>

#define TAB 4

int main()
{
    int c, i = 0, j;
    while ((c = getchar()) != EOF) {
        if (c == ' ') {
            ++i;                // Whitespace count

            if ((i % TAB) == 0) {
                putchar('\t');
            }
        }

        else {
            for(j=0; j < (i % TAB); ++j) {              // every group is smaller that 4 is untouched.
                putchar(' ');
            }
            putchar(c);

            if (i != 0) {
                i = 0;
            }
        }
    }

    return 0;
}


