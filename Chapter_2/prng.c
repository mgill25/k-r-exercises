#include <stdio.h>
// A Pseudo-Random Number Generator
// Results will always be the same for a given seed

int rand(void);                 // rand: return pseudo-random number generator on 0..32767
void srand(unsigned int seed);  // set seed for rand()

unsigned long int next = 1;

int main()
{
    // main logic
    srand(0);           // seed the random number generator
    int i;
    
    for(i = 0; i < 10; i++) {
        printf("A random number: %d\n", rand());
    }

    return 0;
}

int rand(void)
{
    next = next * 1103515245 + 12345;
    return (unsigned int)(next / 65535) % 32768;
}

void srand(unsigned int seed)
{
    next = seed;
}

