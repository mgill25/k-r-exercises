/* Count the number of 1-bits in an integer */

#include <stdio.h>

int bitcount(unsigned x)
{
    /* Declaring the argument x to be unsigned ensures that
     * when it is right-shifted, vacated bits will be filled
     * with zeros, not sign bits, regardless of the machine
     * the program is run on.
     */
    int b;

    for (b = 0; x != 0; x >>= 1) {
        if (x & 01) {
            b++;
        }
    }

    return b;
}

int main()
{
    unsigned a = 10;
    unsigned b = 14;
    unsigned c = 2;

    printf("Bitcount(%u)\t: %d\n", a, bitcount(a));
    printf("Bitcount(%u)\t: %d\n", b, bitcount(b));
    printf("Bitcount(%u)\t: %d\n", c, bitcount(c));
    
    return 0;
}
