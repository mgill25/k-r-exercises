/* Rewrite the function `lower` which converts upper case letters 
 * to lower case, with a conditional expression instead if if-else.
 */

#include <stdio.h>

void lower(char word[], int len)
{
    int i;
    for (i = 0; i < len; i++) {
        word[i] = (word[i] >= 'A' && word[i] <= 'Z') ? word[i] + ('a' - 'A') : word[i];
    }
}

int main()
{
    char name[10] = "Manish";

    printf("%s\n", name);
    lower(name, 10);
    printf("%s\n", name);

    return 0;
}
    

