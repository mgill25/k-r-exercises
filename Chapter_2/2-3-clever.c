// Ref: http://clc-wiki.net/wiki/K%26R2_solutions:Chapter_2:Exercise_3

// The brute-force way was a little meh. Can we do this in a better way?

#include <stdio.h>

// A helper function to help around the problem of not having strchr
int hexadecimal_to_int(int c)
{
    char hexalpha[] = "aAbBcCdDeFfF";           // position of alphabets in the string MATTERS here.
    // Eg> hexalpha[3] is "B", so 10 + (3/2) = 11, which is the decimal equivalent of 0xB

    int i;
    int answer = 0;

    for (i = 0; answer == 0 && hexalpha[i] != '\0'; ++i) {
        if (hexalpha[i] == c) {
            answer = 10 + (i / 2);
        }
    }

    return answer;
}

unsigned int htoi(const char s[])
{
    unsigned int answer = 0;
    int i = 0;
    int valid = 1;
    int hexit;

    if (s[i] == '0') {
        ++i;
        if (s[i] == 'x' || s[i] == 'X') {
            ++i;
        }
    }

    while (valid && s[i] != '\0') {
        answer = answer * 16;
        if (s[i] >= '0' && s[i] <= '9') {
            answer +=  (s[i] - '0');
        }
        else {
            hexit = hexadecimal_to_int(s[i]);
            if (hexit == 0) {
                valid = 0;
            }
            else {
                answer += hexit;
            }
        }
        ++i;
        printf("answer: %u\n", answer);
    }
    if (!valid) {
        answer = 0;
    }
    return answer;
}

int main()
{
    printf("Converting a hexadecimal string into an integer...\n");
    char hex_1[10] = "0X123ae";

    printf("Hexadecimal value: %s\n", hex_1);
    int num_1 = htoi(hex_1);
    printf("Numerical value: %d\n", num_1);

    return 0;
}
