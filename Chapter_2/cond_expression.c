#include <stdio.h>

int main()
{
    int n = -90;
    float f = 2.3;

    int z = (n > 0) ? f : n;

    printf("%d\n", z);

    printf("---------------------------\n");

    int i;
    int limit = 30;
    int a[30] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 }; 

    for (i = 0; i < limit; i++) {
        printf("%6d%c", a[i], (i % 10 == 9 || i == limit-1) ? '\n' : ' ');
        // A newline is printed after every 10th element, 
        // and after the n-th element. All other elements 
        // are followed by one blank.
    }

    /*
     * Another good example:
     * printf("You have %d item%s.\n", n, (n == 1) ? "" : "s");
     */

    return 0;

}
    


