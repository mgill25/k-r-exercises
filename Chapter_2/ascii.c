// Print all ASCII characters
#include <stdio.h>

int main()
{
    // char c = 0;
    int i = 0;
    for(i = 0; i <= 128; i++) {
        if (i % 10 == 0) 
            printf("\n");
        printf("%d: %c ", i, i);
    }
    printf("\n");
    return 0;
}
