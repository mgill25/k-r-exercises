#include <string.h>
#include <stdio.h>

/* reverse string s in place */
void reverse(char s[])
{
    int c, i, j;

    for (i = 0, j = strlen(s)-1; i < j; i++, j--) {
        c = s[i];
        s[i] = s[j];
        s[j] = c;
    }
}

int main()
{
    char name[10] = "Manish";
    printf("Original: %s\n", name);
    reverse(name);
    printf("Reverse: %s\n", name);
    
    char palindrome[6] = "kayak";
    printf("Original: %s\n", palindrome);
    reverse(palindrome);
    printf("Reverse: %s\n", palindrome);

    return 0;
}
