#include <stdio.h>

int lower(int c);       // convert c to lower case: ASCII only

int main()
{
    
    char c = 'P';
    printf("upper: %c\n", c);
    printf("lower: %c\n", lower(c));

    char a = 'a';
    printf("lower: %c\n", lower(a));


    return 0;
}

int lower(int c)
{
    if (c >= 'A' && c <= 'Z') { 
        return c + 'a' - 'A';       // 'a' comes AFTER 'A' in the ASCII table.
    }
    else {
        return c;
    }
}
