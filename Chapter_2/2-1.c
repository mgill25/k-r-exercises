/*
 * Program to determine the ranges of char, short, int and long variables (both signed and unsigned),
 * by printing appropriate values from standard headers and by direct computation.
 * Harder if you compute them: determine the ranges of the various floating-point types.
 */
 // NOTE: the ~ operator is bitwise complement operator
#include <stdio.h>
#include <limits.h>
#include <float.h>

void predefined();
void calculated();

int main()
{
    predefined();
    calculated();

    return 0;
}

void predefined()
{
    printf("Predefined Values...\n\n");

    printf("\tbits in a char: %d\n", CHAR_BIT);
    printf("\tSCHAR_MIN: %d\n", SCHAR_MIN);
    printf("\tSCHAR_MAX: %d\n", SCHAR_MAX);

    printf("\tSHRT_MAX: %d\n", SHRT_MAX);
    printf("\tSHRT_MIN: %d\n", SHRT_MIN);
    printf("\tUSHRT_MAX: %u\n", USHRT_MAX);

    printf("\tINT_MAX: %d\n", INT_MAX);
    printf("\tINT_MIN: %d\n", INT_MIN);

    printf("\tLONG_MAX: %ld\n", LONG_MAX);
    printf("\tLONG_MIN: %ld\n", LONG_MIN);

    printf("\tULONG_MAX: %lu\n", ULONG_MAX);

}

void calculated()
{
    // Ref: http://www.cprogramming.com/tutorial/bitwise_operators.html  for The Bitwise Compliment 
    printf("\n\nCalculated Values...\n\n");

    /* integer types */
    unsigned short s;
    unsigned int i;
    unsigned long l;
    unsigned long long ll;
    /* char type */
    unsigned char c;

    // char
    c = ~0;                                     // Make C a bitwise compliment of 0 => all bits of c are set to 1.
    printf("char c: %d\n", c);                  // Notice that this will be max unsigned value.
    c >>= 1;                                    // halving it to get signed values! :)

    printf("signed char\nmin: %4d,\nmax: %4d\n", -c-1, c);
    printf("unsigned char\nmin: %4u,\nmax: %4u\n", 0, c*2+1);
    printf("----------------------------------\n\n");

    // can do the same thing with ints, shorts, longs, and long longs...
    
    // short
    s = ~0;
    printf("short s: %d\n", s);
    s >>= 1;        // shift right by 1.
    printf("signed short\nmin: %6d,\nmax: %6d\n", -s-1, s);
    printf("unsigned short\nmin: %6u,\nmax: %6u\n", 0, s*2+1);
    printf("----------------------------------\n\n");

    // int
    i = ~0;
    printf("int i: %d\n", i);
    i >>= 1;
    printf("signed int\nmin: %11d,\nmax: %11d\n", -i-1, i);
    printf("unsigned int\nmin: %11u,\nmax: %11u\n", 0, i*2+1);
    printf("----------------------------------\n\n");

    // long
    l = ~0;
    l >>= 1;
    printf("signed long\nmin: %11ld,\nmax: %11ld\n", -l-1, l);
    printf("unsigned long\nmin: %11lu,\nmax: %11lu\n", 0l, l*2+1);
    printf("----------------------------------\n\n");

    // long long
    ll = ~0;
    ll >>= 1;
    printf("signed long long\nmin: %20lld,\nmax: %20lld\n", -ll-1, ll);
    printf("unsigned long long\nmin: %20llu,\nmax: %20llu\n", 0ll, ll*2+1);
    printf("----------------------------------\n\n");

}
