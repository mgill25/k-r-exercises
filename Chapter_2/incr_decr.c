#include <stdio.h>

int main()
{
    int n = 5;
    printf("n : %d\n", n);
    n++;
    printf("after n++:  %d\n", n);
    
    int m = 5;
    printf("m: %d\n", m);
    ++m;
    printf("after ++m: %d\n", m);

    return 0;
}
