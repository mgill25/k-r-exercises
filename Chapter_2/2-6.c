/* Write a function setbits(x, p, n, y) that returns x with the 
 * n bits that begin at position p set to the rightmost n bits of y,
 * leaving the other bits unchanged.
 */

#include <stdio.h>

unsigned getbits(unsigned x, int p, int n)
{
    return (x >> (p + 1 - n)) & ~(~0 << n);
}

void asbits(unsigned x, size_t s, int nl)
{
    int i;
    for(i = s*8-1; i>=0; i--) {
        getbits(x, i, 1) ? putchar('1') : putchar('0');
        if (! (i % 4)) putchar(' ');
    }
    if (nl) {
        putchar('\n');
    }
}

unsigned int setbits(unsigned int x, int p, int n, unsigned int y)
{
    unsigned mask = ~(~0 << n);
    unsigned right_most_y = y & mask;
    unsigned left_n = (y & (~0 << n));
    unsigned final = left_n | right_most_y;
    return final;
}

unsigned int setbits2(unsigned x, int p, int n, unsigned y)
{
    unsigned mask = ~(~0 << n);
    unsigned right_mask = (y & mask);
    unsigned shifted_mask = (right_mask << (p + 1 - n));

    printf("Y Right Mask:\n");
    asbits(right_mask, sizeof(right_mask), 1);
    printf("Y Shifted Mask:\n");
    asbits(shifted_mask, sizeof(shifted_mask), 1);

    return (x & ~(mask << (p + 1 - n))) | ((y & mask) << (p + 1 - n));
}

int main()
{
    int off = 27;
    int len = 7;
    //unsigned x = 0x12345678;
    //unsigned y = 0XffffffFF;
    unsigned x = 10;
    unsigned y = 34;

    printf("off: %u   len: %u\n", off, len);
    asbits(x, sizeof(x), 1);
    asbits(y, sizeof(y), 1);
    printf("----------------------------------------------------\n");
    printf("Correct\n");
    asbits(setbits2(x, off, len, y), sizeof(x), 1);
    //printf("%08x %08x %08x\n", x, y, setbits2(x, off, len, y));

    printf("----------------------------------------------------\n");
    printf("Mine\n");
    asbits(setbits(x, off, len, y), sizeof(x), 1);
    //printf("%08x %08x %08x\n", x, y, setbits(x, off, len, y));

    return 0;
}
