/* In a two's complement number system, x &= (x-1) deletes
 * the rightmost 1-bit in x. Explain Why. Use this 
 * observation to write a faster version of bitcount.
 */

/* When we AND, we end up with 0 in the right-most bit,
 * so we loop until all the bits become 0
 */
#include <stdio.h>

int bitcount(unsigned x)
{
    int count;

    for (count = 0; x != 0; (x &= (x-1))) {
        count++;
    }

    /* Using the while loop */
    /* 
     * count = 0;
     * while (x != 0) {
     *    count++;
     *    x &= (x - 1);
     * }
     *
     */

    return count;
}

int main()
{
    unsigned a = 14;
    unsigned b = 23;
    unsigned c = 2;

    printf("bitcount(%u)\t: %d\n", a, bitcount(a));
    printf("bitcount(%u)\t: %d\n", b, bitcount(b));
    printf("bitcount(%u)\t: %d\n", c, bitcount(c));

    return 0;
}
    
