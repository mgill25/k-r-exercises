/* Write an alternate version of squeeze(s1, s2) that deletes each character in
 * s1 that matches any character in the string s2.
 */

#include <stdio.h>

void squeeze(char s1[], char s2[]);

int main(int argc, char const *argv[])
{
    /* main logic */
    char fn[10] = "Manish";
    char ln[10] = "Gill";

    printf("Orig: %s\n", fn);
    squeeze(fn, ln);
    printf("Now: %s\n", fn);

    return 0;
}

void squeeze(char s1[], char s2[])
{
    int i, j, k;
    int match = 0;

    for (i = 0, j = 0; s1[i] != '\0'; ++i)
    {
        match = 0;
        for (k = 0; s2[k] != '\0' && !match; ++k) {
            // find if a character matches
            if (s1[i] == s2[k]) {
                match = 1;
            }
        }
        // Now, squeeze it!
        if (!match) {
            s1[j++] = s1[i];
        }
        printf("%s\n", s1);
    }
    s1[j] = '\0';
}
