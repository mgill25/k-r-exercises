/* Ex.2.3: Write a function htoi(s) which converts a string of hexadecimal digits
 * (including an optional 0x or 0X) into its equivalent integer value. The allowable digits are
 * 0 through 9, a through f, and A through F.
 */

/*
 * NOTE: compile with: gcc -Wall 2-3.c -o htoi -lm
 * in order to link the math library
 */

#include <stdio.h>
#include <math.h>

int htoi(char s[]);
int strlen(char s[]);
void die();

int main()
{
    printf("Converting a hexadecimal string into an integer...\n");
    char hex_1[10] = "0X123ae";

    printf("Hexadecimal value: %s\n", hex_1);
    int num_1 = htoi(hex_1);
    printf("Numerical value: %d\n", num_1);

    return 0;
}

int htoi(char s[])
{
    // Converting a hexadecimal string into an integer
    int i, n;
    n = 0;
    int len = strlen(s) - 2;
    int pos_exp = len - 1;
    int val;

    if (s[0] == '0' && (s[1] == 'x' || s[1] == 'X')) {
        for(i = 2; s[i] != '\0'; ++i) {
            if ((s[i] >= '0' && s[i] <= '9') || (s[i] >= 'a' && s[i] <='f') || (s[i] >='A' && s[i] <='F')) {
                switch (s[i]) {
                    case 'A':
                    case 'a':
                        val = 10;
                        break;
                    case 'B':
                    case 'b':
                        val = 11;
                        break;
                    case 'C':
                    case 'c':
                        val = 12;
                        break;
                    case 'D':
                    case 'd':
                        val = 13;
                        break;
                    case 'E':
                    case 'e':
                        val = 14;
                        break;
                    case 'F':
                    case 'f':
                        val = 15;
                        break;
                    default:
                        val = (s[i] - '0');
                }
                // printf("s[%d]: %c, val: %d\n", i, s[i], val);
                // printf("pos_exp: %d\n", pos_exp);
                // printf("n: %d\n", n);
                // printf("%d * 16 ^ %d\n", val, pos_exp);
                n += val * pow (16, pos_exp);
                pos_exp--;
            }
            else {
                die();
                return -1;
            }
        }
    return n;
    }
    else {
        die();
        return -1;
    }
}

int strlen(char s[])
{
    int i, count = 0;
    for(i = 0; i != '\0'; ++i) {
        count++;
    }

    return count;
}

void die()
{
    printf("Unexpected input! Please re-check.\n");
}
