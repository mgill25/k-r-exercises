/* Write a function any which returns the first location in the string s1 where any character from 
 * the string s2 occurs, or -1 if s1 contains no characters from s2. (The standard library function
 * `strpbrk` does the same job, but returns a pointer to the location.
 */

#include <stdio.h>

int any(char s1[], char s2[]);

int main(int argc, char const *argv[])
{
    char fn[20] = "Manish";
    char ln[20] = "Gill";

    int loc = any(fn, ln);
    printf("Location: %d\n", loc);

    return 0;
}

int any(char s1[], char s2[])
{
    int i, j;
    int location = -1;
    int found = 0;

    for (i = 0; s1[i] != '\0'; i++) {
        for (j = 0; s2[j] != '\0' && !found; j++) {
            if (s1[i] == s2[j]) {
                location = i;
                found = 1;
                break;
            }
        }
        if (found) {
            break;
        }
   }
    
    return location;
}
