#include <stdio.h>

void str_cat(char s[], char t[]); // concatenates t to the end of s; s must be big enough!

int main()
{

    // main
    char fullname[20] = "Manish";
    char lastname[10] = " Gill";
    str_cat(fullname, lastname);
    printf("Full name: %s\n", fullname);

    return 0;
}

void str_cat(char s[], char t[])
{
    int i, j;
    i = j = 0;
    while (s[i] != '\0') {
        // find the end of s
        i++;
    }
    while ((s[i++] = t[j++]) != '\0') {
        ;                                   // copy t
    }
}

