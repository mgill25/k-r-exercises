#include <stdio.h>

void squeeze(char s[], int c);      // delect all occurrances of c from s

int main(){

    //char string[50] = "XMXAXNXIXSXHX XGXIXLXL";
    char string[10] = "ABCAD";
    printf("Original String: %s\n", string);

    squeeze(string, 'A');
    printf("Squeezing A: %s\n", string);

    return 0;
}

void squeeze(char s[], int c)
{
    int i, j;

    for (i = j = 0; s[i] != '\0'; i++) {
        if (s[i] != c) {
            s[j++] = s[i];
        }
        printf("%d: %s\n", i, s);
    }
    s[j] = '\0';
}
