#include <stdio.h>

int main()
{
    int i;
    
    // A for loop
    /*
    for(i=0; i < lim - 1 && (c = get_char()) != '\n' && c != EOF; ++i) {
        a[i] = c;
    }
    */

    // An equivalent loop without using && or ||
    i = 0;
    while ((c = getchar()) != EOF) {
        if (c == '\n') {
            break;
        }
        else if (i < lim - 1) {
            break;
        }
        a[i] = c;
        ++i;
    }

    // Or better yet...
    while (i < lim - 1) {
        c = get_char();
        if (c == '\n') {
            break;
        }
        else if (c == EOF) {
            break;
        }
        a[i++] = c;
    }      

    return 0;
} 
