/* Get n bits from position p of x.
 * Eg. getbits(x, 4, 3) returns the three bits in
 * bit positions 4, 3 and 2, right adjusted.
 */

#include <stdio.h>

unsigned int getbits(unsigned int x, int p, int n)
{
    return (x >> (p + 1 - n)) & ~(~0 << n);
}

void asbits(unsigned x, size_t s, int nl)
{
    int i;
    for(i = s*8-1; i>=0; i--) {
        getbits(x, i, 1) ? putchar('1') : putchar('0');
        if (! (i % 4)) putchar(' ');
    }
    if (nl) {
        putchar('\n');
    }
}

int main()
{
    unsigned int x = 76;
    int p = 6;
    int n = 6;
    printf("Offset: %d, len: %d\n", p, n);
    printf("%u\n", x);
    asbits(x, sizeof(x), 1);
    printf("%u\n", getbits(x,p,n));
    asbits(getbits(x, p, n), sizeof(x), 1);
    
    return 0;
}
