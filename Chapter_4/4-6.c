/*
 * Exercise 4-6: Add commandso for handling variables. (It's easy to provide twenty-six
 * variables with single-letter names.) Add a variable for the most recently printed value.
 * See <math.h> in Appendix B, Section 4.
 */

#include <stdio.h>
#include <stdlib.h>             /* for atof */
#include <ctype.h>
#include <math.h>
#include <string.h>

#define MAXOP 100               // Max size of operator or operand
#define NUMBER '0'              // Signal that a number was found
#define NAME '1'                // Signal that a keyword was found
#define MAXVAL 100              // Maximum depth of the val stack
#define BUFSIZE 100 
#define VARSIZE 26

// Shared variables
int sp = 0;                     // Next free stack position
int bufp = 0;                   // Buffer pointer
double val[MAXVAL];             // Value stack
char buf[BUFSIZE];              // Input Buffer for getch()/ungetch() 
char SymbolTable[VARSIZE];      
char VarBuffer[VARSIZE];        // Essentially a variable stack
int symp = 0;                   // Pointer for the variable stack

// Function Definitions
int getop(char s[]);
void push(double f);
double pop(void);
int getch(void);                // get a (possibly pushed back) character
void ungetch(int c);            // push character back on input
void sympush(char s[]);
char sympop();

void print_top(void);
void duplicate(void);
void swap_stack(void); 
void clearStack(void);
void handle_math(char s[]);
void handle_vars(char s[]);

// Reverse Polish Calculator
int main()
{
    int type;
    double op2;
    char s[MAXOP];

    while ((type = getop(s)) != EOF) {
        switch (type) {
            case NUMBER:
                push(atof(s));
                break;
            case NAME:
                handle_math(s);
                break;
            case '=':
                break;
            case '+':
                push(pop() + pop());
                break;
            case '-':
                op2 = pop();
                push(pop() - op2);
                break;
            case '*':
                push(pop() * pop());
                break;
            case '/':
                op2 = pop();
                if (op2 == 0.0) {
                    printf("Error: Divide by Zero\n");
                }
                else {
                    push(pop() / op2);
                }
                break; 
            case '%':
                op2 = pop();
                    if (op2 == 0.0) {
                        printf("Error: Modulo by Zero\n");
                    }
                    else {
                        push((double)((int)pop() % (int)op2));
                    }
                break;
            case '?':
                print_top();
                break;
            case '#':
                duplicate();
                break;
            case '~':
                swap_stack();
                break;
            case '!':
                clearStack();
                break;
            case '\n':
                printf("\t%.8g\n", pop());
                break;
            default:
                printf("Error: Unknown Command %s\n", s);
                break;
        }
    }
    return 0;
}

int getop(char s[])
{
    int i, c;
    int next;

    while ((s[0] = c = getch()) == ' ' || c == '\t')
        ;
    s[1] = '\0';

    if (!isdigit(c) && (c == '-' || c == '+')) {
        next = getch();
        if (isdigit(next)) {
            c = next;
        }
        ungetch(next);
    }
    
    i = 0;
    if (isalpha(c)) {
        while (isalpha(s[++i] = c = getch()) && c != '\n') {
            ;
        }
        s[i] = '\0';
        if (c != EOF) {
            ungetch(c);
        }
        return NAME;
    }

    if (!isdigit(c) && c != '.') {
        return c;
    }
    
    if (isdigit(c)) {
        while (isdigit(s[++i] = c = getch())) {
            ;
        }
    }

    if (c == '.') {
        while (isdigit(s[++i] = c = getch())) {
            ;
        }
    }

    s[i] = '\0';
    if (c != EOF) {
        ungetch(c);
    }
    return NUMBER;
}


void push(double f)
{
    if (sp < MAXVAL) {
        val[sp++] = f;
    }
    else {
        printf("Error: Stack full, can't push %g\n", f);
    }
}

double pop()
{
    if (sp > 0) {
        return val[--sp];
    }
    else {
        printf("Error: Stack empty.\n");
        return 0.0;
    }
}

int getch(void) 
{
    return (bufp > 0) ? buf[--bufp] : getchar();
}

void ungetch(int c)
{
    if (bufp >= BUFSIZE) {
        printf("ungetch: Too many characters\n");
    }
    else {
        buf[bufp++] = c;
    }
}

void print_top(void)
{
    if (sp > 0 && sp < MAXVAL) {
        printf("[TOP]:\t%g\n", val[sp-1]);
    }
    else if (sp < 0){
        printf("Error: Stack Empty\n");
    }
    else if (sp > MAXVAL){
        printf("Error: Stack Overflow\n");
    }
}

void duplicate(void)
{
    double tmp = pop();
    push(tmp);
    push(tmp);
}

void swap_stack(void)
{
    double a = pop();
    double b = pop();
    push(b);
    push(a);
}

void clearStack(void)
{
    /* pop() only returns a value when sp > 0, so setting sp to 0 will
     * cause pop() to return its error.
     */
    sp = 0;
}

void handle_math(char s[])
{
    if (strcmp(s, "sin") == 0) {
        push(sin(pop()));
    }
    else if (strcmp(s, "cos") == 0) {
        push(cos(pop()));
    }
    else if (strcmp(s, "tan") == 0) {
        push(tan(pop()));
    }
    else if (strcmp(s, "pow") == 0) {
        push(pow(pop(), pop()));
    }
    else if (strcmp(s, "exp") == 0) {
        push(exp(pop()));
    }
    else if (strlen(s) == 1 && isalpha(s[0])) {
        handle_vars(s);
    }
    /*
    else if (isalpha(s[0])) {
        int index = 'a' - (char)s[0];
        VarBuffer[index] = 1;
        sympush(s);
    }
    */
    else {
        printf("Error: Unknown command: %s\n", s);
    }
}

void handle_vars(char s[])
{
    /* Here's how variables will be handled in this program:
     * VarBuffer[26] is a stack of variables.
     * SymbolTable[26] has the values of variables with corresponding indices.
     * We're essentially trying to do what should ideally be the job of hash tables
     * using two arrays. 
     * If they do exist, they'll be used in all the operations.
     * Something like
     * 10 a =           // assign the value 10 to a
     * 20 a +           // add the value 20 to the value of a.
     * 30 b =
     * a b *            // Should output 300
     */
    sympush(s);
    int variable = (char)s[0];
    int value = pop();
    if (isdigit(value)) {
        SymbolTable[variable] = value; 
    }
}

void sympush(char s[])
{
    if (symp < VARSIZE) {
        VarBuffer[symp++] = (char)s[0];
    }
    else {
        printf("SymbolTable: Size Exceeded!\n");
    }
}

char sympop() 
{
    if (symp > 0) {
        return VarBuffer[--symp];
    }
    else {
        printf("Error: SymbolTable Empty\n");
        return '\0';
    }
}

