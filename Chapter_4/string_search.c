/* Simple use case of grep. Search for literal patterns in a string.
 * while (there's another line) 
 *      if (the line contains the pattern)
 *          print it
 */

#include <stdio.h>

#define MAXLINE 1000                /* maximum input line length */

int get_line(char line[], int max);
int strindex(char source[], char search_pattern[]);

char pattern[] = "ould";            // pattern to search for

// find all lines matching the pattern
int main()
{
    char line[MAXLINE];
    int found = 0;

    while (get_line(line, MAXLINE) > 0) {
        if (strindex(line, pattern) >= 0) {
            printf("%s", line);
            found++;
        }
    }
    return found;
}

int get_line(char s[], int lim)
{
    int c, i;
    i = 0;
    while (--lim > 0 && (c = getchar()) != EOF && c != '\n') {
        s[i++] = c;
    }

    if (c == '\n') {
        s[i++] = c;
    }
    s[i] = '\0';
    return i;
}

/* strindex: return index of pat in str, -1 if none */
int strindex(char str[], char pat[]) 
{
    int i, j, k;
    for (i = 0; str[i] != '\0'; i++) {
        for (j = i, k = 0; pat[k] != '\0' && str[j] == pat[k]; j++, k++) {
            ;
        }
        if (k > 0 && pat[k] == '\0') {
            return i;
        }
    }
    return -1;
}
        
