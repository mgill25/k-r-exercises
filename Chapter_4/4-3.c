/* Ex.4-3: Given the basic framework, it's straightforward to extend the calculator.
 * Add the modulus(%) operator, and provide provisions for negative numbers.
 */

/*
 * Reverse Polish Notation for an infix expression
 * s: (1 - 2) * (4 + 5) 
 * is
 * 1 2 - 4 5 + *
 * Each operand is pushed onto a stack; when an operator arrives the 
 * proper number of operators (2 for binary operations) is popped,
 * the operator is applied to them, and the result is pushed back 
 * onto the stack
 */

#include <stdio.h>
#include <stdlib.h>         // for atof()
#include <ctype.h>

#define MAXOP 100           // max size of operand or operator
#define NUMBER '0'          // signal that a number was found
#define MAXVAL 100          // maximum depth of the val stack
#define BUFSIZE 100         // buffer size for getch() and ungetch()

// Shared Variables
int sp = 0;                 // next free stack position
double val[MAXVAL];         // value stack
char buf[BUFSIZE];          // buffer for ungetch
int bufp = 0;               // next free position in the buffer

// Function declarations
int getop(char s[]);
void push(double);
double pop(void);
int getch(void);
void ungetch(int);

// reverse Polish calculator
int main()
{
    int type;
    double op2;
    char s[MAXOP];

    while ((type = getop(s)) != EOF) {
        switch(type) {
            case NUMBER:
                push(atof(s));
                break;
            case '+':
                push(pop() + pop());
                break;
            case '*':
                push(pop() * pop());
                break;
            case '-':
                op2 = pop();
                push(pop() - op2);
                break;
            case '/':
                op2 = pop();
                if (op2 != 0.0) {
                    push(pop() / op2);
                }
                else {
                    printf("Error: Zero Divisor.\n");
                }
                break;
            case '%':
                op2 = pop();
                if (op2 != 0.0) {
                    push((double)((int)pop() % (int)op2));
                }
                else {
                    printf("Error: Modulo by Zero.\n");
                }
                break;
            case '\n':
                printf("\t%.8g\n", pop());
                break;
            default:
                printf("Error: Unknown Command %s\n", s);
                break;
        }
    }
    
    return 0;
}

// push: push f onto the stack
void push(double f)
{
    if (sp < MAXVAL) {
        val[sp++] = f;
    }
    else {
        printf("Error: Stack full, can't push %g\n", f);
    }
}

// pop: pop and return top value from stack
double pop(void)
{
    if (sp > 0) {
        return val[--sp];
    }
    else {
        printf("Error: Stack empty.\n");
        return 0.0;
    }
}

// getop: get next operator or numeric operand
int getop(char s[])
{
    int i, c;
    int next;
    
    // All whitespaces are assigned to s[0]
    while ((s[0] = c = getch()) == ' ' || c == '\t') {
        ;
    }
    s[1] = '\0';
    i = 0;
    
    /* Negative Numbers
     * We 'look ahead' to see that the character after the '-' is a number or not.
     * Also, can now use the '+' symbol to represent positive numbers as well! :)
     */
    if (!isdigit(c) && (c == '-' || c == '+')) {
        next = getch();
        if (isdigit(next)) {
            c = next;
        }
        ungetch(next);
    }

    if (!isdigit(c) && c != '.') {
        return c;           // not a number
    }
     
    if (isdigit(c)) {
        // collect integer part
        while (isdigit(s[++i] = c = getch())) {
            ;
        }
    }

    if (c == '.') {
        while (isdigit(s[++i] = c = getch())) {
            ;
        }
    }
    
    s[i]  = '\0';
    if (c != EOF) {
        ungetch(c);
    }
    return NUMBER;
}

int getch(void) 
{
    // get a (possibly pushed back) character
    return (bufp > 0) ? buf[--bufp] : getchar();
}

void ungetch(int c) 
{
    // push character back on input
    if (bufp >= BUFSIZE) {
        printf("ungetch: too many characters\n");
    }
    else {
        buf[bufp++] = c;
    }
}

