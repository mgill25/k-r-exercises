/*
 * Write the function `strrindex(s, t), which returns the position
 * of the `rightmost` occurrene of t in s, or -1 if there is none. 
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAXLINE 1000

int get_line(char line[], int limit); 
int strrindex(char string[], char pattern[]);

int main()
{
    char line[MAXLINE];
    char pattern[10] = "test"; 
    int found = 0;
    int pos;
    while (get_line(line, MAXLINE) > 0) {
        if ((pos = strrindex(line, pattern)) > 0) {
            printf("pattern occurred at index: %d\n", pos);
            printf("%s", line);
            found++;
        }
    }

    return found; 
}

int get_line(char line[], int limit)
{
    int c, i = 0;
    while (--limit > 0 && (c = getchar()) != EOF && c != '\n') {
        line[i++] = c;
    }
    
    if (c == '\n') {
        line[i++] = c;
    }

    line[i] = '\0';

    return i;
}

int strrindex(char s[], char pat[])
{
    int i, j, k;
    for (i = strlen(s) - strlen(pat) - 1; i >= 0; i--) {
        for (j = i, k = 0; pat[k] != '\0' && (s[j] == pat[k]); j++, k++) {
            ;
        }
        if (k > 0 && pat[k] == '\0') {
            return i;
        }
    }
    return -1;
}
