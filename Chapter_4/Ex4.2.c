/*
 * Extend atof to handle scientific notation of the form
 * 123.45e-6
 * where a floating point number may be followed by e or E and 
 * an optionally signed exponent
 */
#include <stdio.h>
#include <ctype.h>
#include <math.h>

double atof(char s[])
{
    double val, power;
    int i, sign;
    int exp_sign; 

    for (i = 0; isspace(s[i]); i++) {
        ;
    }
    
    sign = (s[i] == '-') ? -1 : 1;
    if (s[i] == '-' || s[i] == '+') {
        i++;
    }

    for (val = 0.0; isdigit(s[i]); i++) {
        val = 10.0 * val + (s[i] - '0');
    }
    if (s[i] == '.') {
        i++;
    }
    
    for(power = 1.0; isdigit(s[i]); i++) {
        val = 10.0 * val + (s[i] - '0');
        power *= 10.0;
    }
    if (s[i] == 'e') {
        i++;
    }
    
    exp_sign = (s[i] == '-') ? -1 : 1;
    if (s[i] == '-' || s[i] == '+') {
        i++;
    }
    int exponent = 0;
    for (exponent = 0; isdigit(s[i]); i++) {
        exponent = 10 * exponent + (s[i] - '0');
    }
    
    //printf("Exponent: %d\n", exponent);
    //printf("%f\n", expt(10, exponent));
    //printf("power: %f\n", power);
    //printf("val: %f\n", val);
    return sign * val/power * pow(10, exp_sign*exponent);
}

int main()
{
    char scientific[20] = "123.45e-6";
    double sci_doub = atof(scientific);
    printf("String: %s\n", scientific);
    printf("Double: %8.8f\n", sci_doub);

    return 0;
}


