/*
 * Exercise 4-4: Add Commands to print the top element of the stack 
 * without popping, to duplicate it, and to swap the top two elements. 
 * Add  a command to clear the stack.
 */

#include <stdio.h>
#include <stdlib.h>             /* for atof */
#include <ctype.h>

#define MAXOP 100               // Max size of operator or operand
#define NUMBER '0'              // Signal that a number was found
#define MAXVAL 100              // Maximum depth of the val stack
#define BUFSIZE 100 

// Shared variables
int sp = 0;                     // Next free stack position
int bufp = 0;                   // Buffer pointer
double val[MAXVAL];             // Value stack
char buf[BUFSIZE];              // Input Buffer for getch()/ungetch() 
// Function Definitions
int getop(char s[]);
void push(double f);
double pop(void);
int getch(void);                // get a (possibly pushed back) character
void ungetch(int c);            // push character back on input

void print_top(void);
void duplicate(void);
void swap_stack(void); 
void clearStack(void);

// Reverse Polish Calculator
int main()
{
    int type;
    double op2;
    char s[MAXOP];

    while ((type = getop(s)) != EOF) {
        switch (type) {
            case NUMBER:
                push(atof(s));
                break;
            case '+':
                push(pop() + pop());
                break;
            case '-':
                op2 = pop();
                push(pop() - op2);
                break;
            case '*':
                push(pop() * pop());
                break;
            case '/':
                op2 = pop();
                if (op2 == 0.0) {
                    printf("Error: Divide by Zero\n");
                }
                else {
                    push(pop() / op2);
                }
                break; 
            case '%':
                op2 = pop();
                    if (op2 == 0.0) {
                        printf("Error: Modulo by Zero\n");
                    }
                    else {
                        push((double)((int)pop() % (int)op2));
                    }
                break;
            case '?':
                print_top();
                break;
            case '#':
                duplicate();
                break;
            case '~':
                swap_stack();
                break;
            case '!':
                clearStack();
                break;
            case '\n':
                printf("\t%.8g\n", pop());
                break;
            default:
                printf("Error: Unknown Command %s\n", s);
                break;
        }
    }
    return 0;
}

int getop(char s[])
{
    int i, c;
    int next;

    while ((s[0] = c = getch()) == ' ' || c == '\t')
        ;
    s[1] = '\0';

    if (!isdigit(c) && (c == '-' || c == '+')) {
        next = getch();
        if (isdigit(next)) {
            c = next;
        }
        ungetch(next);
    }
    if (!isdigit(c) && c != '.') {
        return c;
    }

    i = 0;
    if (isdigit(c)) {
        while (isdigit(s[++i] = c = getch())) {
            ;
        }
    }
    if (c == '.') {
        while (isdigit(s[++i] = c = getch())) {
            ;
        }
    }

    s[i] = '\0';
    if (c != EOF) {
        ungetch(c);
    }
    return NUMBER;
}


void push(double f)
{
    if (sp < MAXVAL) {
        val[sp++] = f;
    }
    else {
        printf("Error: Stack full, can't push %g\n", f);
    }
}

double pop()
{
    if (sp > 0) {
        return val[--sp];
    }
    else {
        printf("Error: Stack empty.\n");
        return 0.0;
    }
}

int getch(void) 
{
    return (bufp > 0) ? buf[--bufp] : getchar();
}

void ungetch(int c)
{
    if (bufp >= BUFSIZE) {
        printf("ungetch: Too many characters\n");
    }
    else {
        buf[bufp++] = c;
    }
}

void print_top(void)
{
    if (sp > 0 && sp < MAXVAL) {
        printf("[TOP]:\t%g\n", val[sp-1]);
    }
    else if (sp < 0){
        printf("Error: Stack Empty\n");
    }
    else if (sp > MAXVAL){
        printf("Error: Stack Overflow\n");
    }
}

void duplicate(void)
{
    double tmp = pop();
    push(tmp);
    push(tmp);
}

void swap_stack(void)
{
    double a = pop();
    double b = pop();
    push(b);
    push(a);
}

void clearStack(void)
{
    /* pop() only returns a value when sp > 0, so setting sp to 0 will
     * cause pop() to return its error.
     */
    sp = 0;
}

